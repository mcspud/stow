# For inspiration: https://github.com/antoineco/dotfiles/blob/901a5ae6f4cb6f6f810b9657596708f614c4de96/flake.nix#L376-L393


{
  description = "Nix-darwin flakes with device profiles";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    nix-darwin.url = "github:LnL7/nix-darwin";
    nix-darwin.inputs.nixpkgs.follows = "nixpkgs";
    mac-app-util.url = "github:hraban/mac-app-util";
    nix-homebrew.url = "github:zhaofengli-wip/nix-homebrew";
    neovim-nightly-overlay.url = "github:nix-community/neovim-nightly-overlay";
  };

  outputs =
    inputs@{ self
    , nixpkgs
    , nix-darwin
    , mac-app-util
    , nix-homebrew
    , neovim-nightly-overlay
    }:
    let
      # Required for formatting on different instruction sets
      systems = [ "x86_64-linux" "aarch64-linux" "aarch64-darwin" "x86_64-darwin" ];

      # Shared packages that will be available on all systems
      sharedPackages = pkgs: with pkgs; [
        vim
        sketchybar
        tokei
        qbittorrent
        vscodium
        delta
        nodejs

        # Browsers
        brave

        # Shell 
        atuin
        erdtree
        eza
        ripgrep
        fd
        fzf
        gitui
        glab
        mas
        starship
        stow
        tmux
        yazi
        zellij
        zoxide
        jq
        aerospace
        btop

        # Emulators
        wezterm
        tree-sitter

        # Apps
        flameshot
        signal-desktop
        discord

        # Config
        dockutil

        # Tools
        uv
        python311Packages.yt-dlp
        ffmpeg
        imagemagick
        restic

        # Emulator
        nushell
        zsh-autosuggestions
        zsh-autocomplete
        zsh-syntax-highlighting

        # LSP
        nil
        nixpkgs-fmt

      ];

      # Shared homebrew casks for all systems
      sharedCasks = [
        "istat-menus"
        "bartender"
        "alfred"
        "vlc"
        "adguard"
      ];

      # Shared Mac App Store apps for all systems
      sharedMasApps = {
        "Strongbox" = 897283731;
        "SurfShark" = 1437809329;
        "GrandPerspective" = 1111570163;
        "Clocker" = 1056643111;
        "WhatsApp Messenger" = 310633997;
      };

      # Common configuration that is shared across all profiles
      commonConfiguration = { pkgs, ... }: {
        nixpkgs.config.allowUnfree = true;


        nixpkgs.overlays = [
          inputs.neovim-nightly-overlay.overlays.default
        ];

        environment.systemPackages = sharedPackages pkgs;

        system.activationScripts.postUserActivation.text = ''
          echo "Configuring dock items..."
          # Remove all persistent apps from Dock
          ${pkgs.dockutil}/bin/dockutil --remove all --no-restart
        
          # Add apps to Dock
          ${pkgs.dockutil}/bin/dockutil --add "/System/Applications/Messages.app" --no-restart
          ${pkgs.dockutil}/bin/dockutil --add "/${pkgs.lib.getBin pkgs.signal-desktop}/Signal.app" --no-restart
          ${pkgs.dockutil}/bin/dockutil --add "/${pkgs.lib.getBin pkgs.discord}/Discord.app" --no-restart
          ${pkgs.dockutil}/bin/dockutil --add "/Applications/WhatsApp.app" --no-restart
        
          # Needed for aerospace
          echo "Setting workspace auto-swoosh..."
          defaults write com.apple.dock workspaces-auto-swoosh -bool NO

          echo "Setting wallpaper..."
          osascript -e "tell application \"Finder\" to set desktop picture to POSIX file \"$HOME/wallpaper2.jpg\""

          # Set screensaver idle time to 5 minutes (300 seconds)
          echo "Setting screensaver idle time..."
          defaults -currentHost write com.apple.screensaver idleTime -int 300

          # These are under Lock Screen in the settings menu
          echo "Setting power management settings..."
          # Display sleep settings (in minutes)
          # Battery
          sudo pmset -b displaysleep 5
          # Power Adapter
          sudo pmset -c displaysleep 5
        '';

        programs.zsh = {
          enable = true;
          enableCompletion = true;
          enableBashCompletion = true;
          promptInit = "";
          interactiveShellInit = ''
            # Load plugins
            source ${pkgs.zsh-syntax-highlighting}/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
            source ${pkgs.zsh-autosuggestions}/share/zsh-autosuggestions/zsh-autosuggestions.zsh
            source ${pkgs.zsh-autocomplete}/share/zsh-autocomplete/zsh-autocomplete.plugin.zsh
          '';
        };

        launchd.user.agents = {
          flameshot = {
            serviceConfig = {
              ProgramArguments = [ "/${pkgs.lib.getBin pkgs.flameshot}/bin/flameshot" ];
              KeepAlive = true;
              RunAtLoad = true;
              StandardOutPath = "/tmp/flameshot.log";
              StandardErrorPath = "/tmp/flameshot.error.log";
            };
          };

          sketchybar = {
            serviceConfig = {
              ProgramArguments = [ "${pkgs.sketchybar}/bin/sketchybar" ];
              RunAtLoad = true;
              KeepAlive = true;
              StandardOutPath = "/tmp/sketchybar.log";
              StandardErrorPath = "/tmp/sketchybar.error.log";
              ProcessType = "Interactive";
            };
          };

          discord = {
            serviceConfig = {
              ProgramArguments = [ "/${pkgs.lib.getBin pkgs.discord}/Discord.app/Contents/MacOS/Discord" ];
              KeepAlive = false;
              RunAtLoad = true;
              StandardOutPath = "/tmp/discord.log";
              StandardErrorPath = "/tmp/discord.error.log";
            };
          };

          signal = {
            serviceConfig = {
              ProgramArguments = [ "/${pkgs.lib.getBin pkgs.signal-desktop}/Signal.app/Contents/MacOS/Signal" ];
              KeepAlive = false;
              RunAtLoad = true;
              StandardOutPath = "/tmp/signal.log";
              StandardErrorPath = "/tmp/signal.error.log";
            };
          };

          bartender = {
            serviceConfig = {
              ProgramArguments = [ "/Applications/Bartender 5.app/Contents/MacOS/Bartender 5" ];
              KeepAlive = false;
              RunAtLoad = true;
              StandardOutPath = "/tmp/bartender.log";
              StandardErrorPath = "/tmp/bartender.error.log";
            };
          };

          istat-menus = {
            serviceConfig = {
              ProgramArguments = [ "/Applications/iStat Menus.app/Contents/MacOS/iStat Menus" ];
              KeepAlive = false;
              RunAtLoad = true;
              StandardOutPath = "/tmp/istat-menus.log";
              StandardErrorPath = "/tmp/istat-menus.error.log";
            };
          };
        };


        fonts.packages = with pkgs; [
          nerd-fonts.hasklug
          nerd-fonts.hack
        ];

        nix.settings.experimental-features = "nix-command flakes";
        # services.nix-daemon.enable = true;
        system.configurationRevision = self.rev or self.dirtyRev or null;
        system.stateVersion = 5;

        system.keyboard = {
          enableKeyMapping = true;
          userKeyMapping = [
            {
              HIDKeyboardModifierMappingSrc = 30064771129; # Caps Lock
              HIDKeyboardModifierMappingDst = 30064771299; # Command
            }
          ];
        };

        system.defaults = {
          NSGlobalDomain = {
            KeyRepeat = 1;
            InitialKeyRepeat = 10;
          };

          finder = {
            AppleShowAllExtensions = true;
            # Show with . prefix
            AppleShowAllFiles = true;
            # Column view
            FXPreferredViewStyle = "clmv";
            # Arrange by type
            _FXSortFoldersFirst = true;
          };

          spaces = {
            # Needed for sketchybar
            spans-displays = false;
          };

          dock = {
            autohide = false;
            mru-spaces = false;
            show-recents = false;
            minimize-to-application = true;
            orientation = "left";
            tilesize = 32;
            largesize = 40;
            magnification = false;
          };

          controlcenter = {
            Bluetooth = true;
            NowPlaying = true;
            Sound = true;
          };

          loginwindow = {
            LoginwindowText = "super happy adventure fun time";
            GuestEnabled = false;
          };

          screencapture.location = "~/Pictures/screenshots";

          screensaver = {
            # Require password after screensaver begins
            askForPassword = true;
            # Delay in seconds before password is required once screensave is active
            askForPasswordDelay = 0;
          };
        };
      };

      # Creates an Apple(Darwin) system with profile-specific configurations
      mkDarwinSystem = profileName:
        let
          profile = profiles.${profileName};
        in
        nix-darwin.lib.darwinSystem {
          modules = [
            commonConfiguration
            ({ pkgs, ... }: {
              nixpkgs.hostPlatform = "aarch64-darwin";
              networking.hostName = profileName;

              # Profile-specific packages
              environment.systemPackages = profile.extraPackages pkgs;
            })
            mac-app-util.darwinModules.default
            nix-homebrew.darwinModules.nix-homebrew
            {
              nix-homebrew = {
                enable = true;
                user = profile.user;
                mutableTaps = true;
                autoMigrate = true;
              };
              homebrew = {
                enable = true;
                onActivation = {
                  autoUpdate = true;
                  cleanup = "uninstall";
                  upgrade = true;
                };
                casks = sharedCasks ++ profile.extraCasks;
                masApps = sharedMasApps // profile.extraMasApps;
              };
            }
          ];
        };

      # Available profiles.  
      # Any device-specific configurations add them to here
      profiles = {
        m3air = {
          user = "jamie";
          extraPackages = _: [ ];
          extraCasks = [ ];
          extraMasApps = {
            "Reeder Classic." = 1529448980;
          };
        };

        m1max = {
          user = "jamie";
          extraPackages = pkgs: [
            pkgs.dotnet-sdk_9
            pkgs.elixir
            pkgs.erlang

            # Containers
            pkgs.podman
            pkgs.podman-compose
            pkgs.podman-desktop
          ];
          extraCasks = [
            "dotnet"
            "gh"
          ];
          extraMasApps = {
            "Fantastical" = 975937182;
          };
        };
      };

    in
    {

      formatter = nixpkgs.lib.genAttrs systems (system: nixpkgs.legacyPackages.${system}.nixpkgs-fmt);

      darwinConfigurations = builtins.mapAttrs
        (name: _: mkDarwinSystem name)
        profiles;
    };
}
