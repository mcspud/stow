alias t = tmux
alias c = clear
alias n = nvim
alias cat = bat
alias lsa = exa -lhaL --icons --group-directories-first
alias tree = et --icons --hidden
alias docker-compose = docker compose

# Mainly used for Fedora so I don't need to fuck around with remembering it, use `pbcopy` for mac
alias copy = xclip -sel clip

# git

alias gco = git checkout
alias gcob = git checkout -b
alias gs = git status
alias gp = git push
alias gc = git commit
alias gfa = git fetch --all --prune
alias gpsup = git push --set-upstream origin 
alias grt = cd (git rev-parse --show-toplevel)

# Shamelessly stolen from https://stackoverflow.com/a/70453420 - Thank you kind nerd
# alias git-ssh = git remote set-url origin $(git remote get-url origin | sed -E '\'s,^https://([^/]*)/(.*)$,git@\1:\2,'\')
# alias git-https = git remote set-url origin $(git remote get-url origin | sed -E '\'s,^git@([^:]*):/*(.*)$,https://\1/\2,'\')
