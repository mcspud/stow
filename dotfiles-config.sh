#!/usr/bin/env bash

set -euo pipefail

# Atuin is a PITA and we need to aggressively nuke the config before next tick
rm -rf ~/.config/atuin && stow --target $HOME atuin
stow --target $HOME aerospace
stow --target $HOME bat
stow --target $HOME erdtree
stow --target $HOME flameshot
stow --target $HOME git
stow --target $HOME gitui
stow --target $HOME navi
stow --target $HOME nvim
stow --target $HOME restic
stow --target $HOME sketchybar
stow --target $HOME starship
stow --target $HOME tmux
stow --target $HOME wezterm
stow --target $HOME yazi
stow --target $HOME zellij
stow --target $HOME zsh
