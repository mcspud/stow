#!/usr/bin/env zsh

# Enable zsh options for error handling
setopt ERR_EXIT
setopt PIPE_FAIL
setopt UNSET

# Function to safely get password store values
get_pass() {
    pass "$1" 2>/dev/null || echo ""
}

# Set variables
RESTIC_HOST=$(hostname)
RESTIC_REPOSITORY=$(get_pass "$RESTIC_HOST/RESTIC_REPOSITORY")
B2_ACCOUNT_ID=$(get_pass "$RESTIC_HOST/B2_ACCOUNT_ID")
B2_ACCOUNT_KEY=$(get_pass "$RESTIC_HOST/B2_ACCOUNT_KEY")
RESTIC_PASSWORD_COMMAND="pass $RESTIC_HOST/RESTIC_PASSWORD"

# Export variables
export RESTIC_HOST RESTIC_REPOSITORY B2_ACCOUNT_ID B2_ACCOUNT_KEY RESTIC_PASSWORD_COMMAND

# Print variables for debugging (optional)
print "RESTIC_HOST: $RESTIC_HOST"
print "RESTIC_REPOSITORY: $RESTIC_REPOSITORY"
print "B2_ACCOUNT_ID: $B2_ACCOUNT_ID"
print "B2_ACCOUNT_KEY: $B2_ACCOUNT_KEY"
print "RESTIC_PASSWORD_COMMAND: $RESTIC_PASSWORD_COMMAND"
