## Stow

`sh dotfiles-config.sh` to dump all of your config files back

## nix

Profiles are set up for each device

` darwin-rebuild switch --flake .#m3air`

` darwin-rebuild switch --flake .#m1max`

Clean up your nix cache fairly often (who rolls back anyway)

` nix-clear`

### Update

`nix flake update`

Run the clean command afterwards

### Install

`sh <(curl -L https://nixos.org/nix/install)`

## Brew

Sometimes Brew whinges, fix it with:

`sudo chown -R $(whoami):admin /opt/homebrew`


