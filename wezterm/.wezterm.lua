-- Pull in the wezterm API
local wezterm = require("wezterm")

-- This table will hold the configuration.
local config = {}

-- term =
config.term = "wezterm"

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
	config = wezterm.config_builder()
end

-- This is where you actually apply your config choices

config.color_scheme = "hardhacker"
config.default_prog = { "/bin/zsh", "-l" }
-- config.default_prog = { "/opt/homebrew/bin/nu" }

-- config.font_locator = 'ConfigDirsOnly'
config.font_dirs = { "fonts" }
config.font = wezterm.font("Hasklug Nerd Font Mono", {
	italic = false,
  bold = false,
  
})
config.front_end = "WebGpu"

config.font_size = 13
config.text_background_opacity = 1.00
-- config.macos_window_background_blur = 20
config.macos_window_background_blur = 10
-- config.cursor_thickness = '1cell'
config.default_cursor_style = "SteadyBlock"
-- config.default_cursor_style = 'BlinkingBlock'
-- config.win32_system_backdrop = 'Acrylic'

-- https://wezfurlong.org/wezterm/config/lua/config/background.html#source-definition
config.background = {
	{
		source = { File = { path = "/Users/jamie/Projects/stow/wezterm/wallpaper4.jpeg" } },
		hsb = {
			brightness = 0.05,
			saturation = 0.90,
		},
		opacity = 1.00,
		horizontal_align = "Center",
		height = "Cover",
		width = "Cover",
	},
	-- {
	--   source = {
	--     Gradient = {
	--       blend = 'Rgb',
	--       interpolation = 'Linear',
	--       orientation = 'Vertical',
	--       colors = {
	--         '#0f0c29',
	--         '#24243e',
	--       },
	--       preset = 'Warm'
	--     }
	--   }
	-- }
}

config.tab_bar_at_bottom = true
config.use_fancy_tab_bar = false
config.colors = {
	-- Overrides the cell background color when the current cell is occupied by the
	-- cursor and the cursor style is set to Block
	cursor_bg = "#DE556F",
	-- Overrides the text color when the current cell is occupied by the cursor
	cursor_fg = "#EEEEEE",

	tab_bar = {
		-- The color of the strip that goes along the top of the window
		-- (does not apply when fancy tab bar is in use)
		background = "#07090B",

		active_tab = {
			bg_color = "#7db6ff",
			fg_color = "#28333e",
			intensity = "Normal", -- "Half", "Normal" or "Bold"
			underline = "None", -- "None", "Single" or "Double"
			italic = false,
			strikethrough = false,
		},

		inactive_tab = {
			bg_color = "#3c4351",
			fg_color = "#8cc570",
			intensity = "Normal", -- "half", "normal" or "bold"
			underline = "None", -- "none", "single" or "double"
			italic = false,
			strikethrough = false,
		},

		inactive_tab_hover = {
			bg_color = "#8cc570",
			fg_color = "#3c4351",
			intensity = "Normal", -- "half", "normal" or "bold"
			underline = "None", -- "none", "single" or "double"
			italic = false,
			strikethrough = false,
		},

		new_tab = {
			bg_color = "#ffaf7f",
			fg_color = "#080808",
			intensity = "Normal", -- "half", "normal" or "bold"
			underline = "None", -- "none", "single" or "double"
			italic = false,
			strikethrough = false,
		},

		new_tab_hover = {
			bg_color = "#8cc570",
			fg_color = "#272c35",
			intensity = "Normal", -- "half", "normal" or "bold"
			underline = "None", -- "none", "single" or "double"
			italic = false,
			strikethrough = false,
		},
	},
}

config.window_padding = {
	left = ".0cell",
	right = ".0cell",
	top = ".0cell",
	bottom = 0,
}
return config
