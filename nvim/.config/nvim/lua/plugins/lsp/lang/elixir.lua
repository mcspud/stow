return {
  {
    'elixir-tools/elixir-tools.nvim',
    dependencies = {
      'nvim-lua/plenary.nvim',
    },
    version = '*',
    event = { 'BufReadPre', 'BufNewFile' },
    config = function()
      local elixir = require 'elixir'
      local elixirls = require 'elixir.elixirls'

      elixir.setup {
        nextls = {
          enable = true,
          cmd = '/home/linuxbrew/.linuxbrew/bin/nextls',
          init_options = {
            experimental = {
              completions = {
                enable = false, -- control if completions are enabled. defaults to false
              },
            },
          },
          -- cmd = "path/to/next-ls"
        },
        credo = { enabled = true },
        elixirls = {
          cmd = 'elixir-ls',
          enable = true,
          settings = elixirls.settings {
            dialyzerEnabled = true,
            enableTestLenses = false,
            suggestSpecs = true,
            fetchDeps = false,
          },
        },
      }
    end,
  },
  {
    'nvim-treesitter/nvim-treesitter',
    opts = function(_, opts)
      vim.list_extend(opts.ensure_installed, {
        'elixir',
        'heex',
        'eex',
      })
    end,
  },
  {
    'neovim/nvim-lspconfig',
    opts = {
      servers = {
        elixirls = {
          -- disable elixirls always
          enabled = true,
        },
        nextls = {
          -- disable elixirls always
          enabled = true,
        },
        lexical = {
          -- TODO waiting for 1.17 suppport
          -- cmd = { '/home/adam/.local/share/nvim/mason/bin/lexical', 'server' },
          -- cmd = { '/home/adam/Repositories/lexical/_build/dev/package/lexical/bin/start_lexical.sh', 'server' },
          -- root_dir = require('lspconfig.util').root_pattern { 'mix.exs' },
        },
        -- elixirls = {},
      },
    },
  },
  {
    'nvim-neotest/neotest',
    optional = true,
    dependencies = {
      'jfpedroza/neotest-elixir',
    },
    opts = {
      adapters = {
        ['neotest-elixir'] = {},
      },
    },
  },
}
