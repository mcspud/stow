local M = {}

function M.on_attach(client, buffer)
  local self = M.new(client, buffer)

  self:map('gd', 'Telescope lsp_definitions', { desc = 'Goto Definition' })
  self:map('gr', 'Telescope lsp_references', { desc = 'References' })
  self:map('K', 'lua vim.lsp.buf.hover()', { desc = 'Hover' })
  self:map('gK', vim.lsp.buf.signature_help, { desc = 'Signature Help', has = 'signatureHelp' })
  self:map('gl', 'Trouble lsp', { desc = 'Trouble LSP' })

  self:map(']d', M.diagnostic_goto(true), { desc = 'Next Diagnostic' })
  self:map('[d', M.diagnostic_goto(false), { desc = 'Prev Diagnostic' })
  self:map(']e', M.diagnostic_goto(true, 'ERROR'), { desc = 'Next Error' })
  self:map('[e', M.diagnostic_goto(false, 'ERROR'), { desc = 'Prev Error' })
  self:map(']w', M.diagnostic_goto(true, 'WARNING'), { desc = 'Next Warning' })
  self:map('[w', M.diagnostic_goto(false, 'WARNING'), { desc = 'Prev Warning' })

  vim.keymap.set('n', '<leader>lp', "<cmd>lua require('chainsaw').variableLog()<cr>", { desc = 'lsp Print' })
  vim.keymap.set('n', '<leader>lc', '<cmd>Telescope luasnip<CR>', { desc = 'lsp Snippets' })
  vim.keymap.set('n', '<leader>lN', function()
    M.rename()
  end, { desc = 'Rename' })
  vim.keymap.set('n', '<leader>lF', '<cmd>lua vim.lsp.buf.format { async = true }<cr>', { desc = 'lsp Format' })
  vim.keymap.set('n', '<leader>la', '<cmd>lua vim.lsp.buf.code_action()<CR>', { desc = 'lsp Code Action' })
  vim.keymap.set('n', '<leader>lL', '<cmd>lua vim.lsp.codelens.refresh()<CR>', { desc = 'lsp Refresh CodeLens' })
  vim.keymap.set('n', '<leader>ll', '<cmd>lua vim.lsp.codelens.run()<CR>', { desc = 'lsp Run CodeLens' })
  vim.keymap.set('n', '<leader>lD', "<cmd>lua require('config.lsp').toggle_diagnostics()<CR>", { desc = 'lsp Toggle Inline Diagnostics' })
  vim.keymap.set('n', '<leader>lo', '<Cmd>DocsViewToggle<CR>', { desc = 'lsp Symbols Outline' })
  vim.keymap.set('n', '<leader>lj', '<cmd>:TSJToggle<CR>', { desc = 'lsp Split Toggle' })
  vim.keymap.set('n', '<leader>lx', "<cmd>lua vim.lsp.stop_client(vim.lsp.get_clients()) vim.cmd('edit')<CR>", { desc = 'Restart LSP' })
end

function M.new(client, buffer)
  return setmetatable({ client = client, buffer = buffer }, { __index = M })
end

function M:has(cap)
  return self.client.server_capabilities[cap .. 'Provider']
end

function M:map(lhs, rhs, opts)
  opts = opts or {}
  if opts.has and not self:has(opts.has) then
    return
  end
  vim.keymap.set(
    opts.mode or 'n',
    lhs,
    type(rhs) == 'string' and ('<cmd>%s<cr>'):format(rhs) or rhs,
    ---@diagnostic disable-next-line: no-unknown
    { silent = true, buffer = self.buffer, expr = opts.expr, desc = opts.desc }
  )
end

function M.rename()
  if pcall(require, 'inc_rename') then
    return ':IncRename ' .. vim.fn.expand '<cword>'
  else
    vim.lsp.buf.rename()
  end
end

function M.diagnostic_goto(next, severity)
  local go = next and vim.diagnostic.goto_next or vim.diagnostic.goto_prev
  severity = severity and vim.diagnostic.severity[severity] or nil
  return function()
    go { severity = severity }
  end
end

return M
