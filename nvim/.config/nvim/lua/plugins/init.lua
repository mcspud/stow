return {
  'nvim-lua/plenary.nvim',
  'MunifTanjim/nui.nvim',

  { 'lambdalisue/suda.vim', lazy = true, event = 'VeryLazy' },

  {
    'numToStr/Navigator.nvim',
    lazy = true,
    event = 'VeryLazy',
    config = function()
      ---@diagnostic disable-next-line: missing-parameter
      require('Navigator').setup()
      vim.keymap.set({ 'n', 't' }, '<C-h>', '<CMD>NavigatorLeft<CR>')
      vim.keymap.set({ 'n', 't' }, '<C-l>', '<CMD>NavigatorRight<CR>')
      vim.keymap.set({ 'n', 't' }, '<C-k>', '<CMD>NavigatorUp<CR>')
      vim.keymap.set({ 'n', 't' }, '<C-j>', '<CMD>NavigatorDown<CR>')
      vim.keymap.set({ 'n', 't' }, '<C-p>', '<CMD>NavigatorPrevious<CR>')
    end,
  },

  {
    'echasnovski/mini.files',
    enabled = false,
    lazy = false,
    version = false,
    config = function()
      require('mini.files').setup {
        windows = {
          -- Maximum number of windows to show side by side
          max_number = math.huge,
          -- Whether to show preview of file/directory under cursor
          preview = true,
          -- Width of focused window
          width_focus = 80,
          -- Width of non-focused window
          width_nofocus = 15,
          -- Width of preview window
          width_preview = 80,
        },
        mappings = {
          close = 'q',
          go_in = 'l',
          go_in_plus = '<CR>',
          go_out = 'h',
          go_out_plus = 'H',
          -- reset = '<BS>',
          reveal_cwd = '@',
          show_help = 'g?',
          synchronize = '=',
          trim_left = '<',
          trim_right = '>',
        },
      }

      -- vim.api.nvim_create_autocmd('User', {
      --   pattern = 'MiniFilesBufferCreate',
      --   callback = function(args)
      --     local buf_id = args.data.buf_id
      --     -- MiniFiles.close()
      --     if not MiniFiles.close() then
      --       MiniFiles.open(vim.api.nvim_buf_get_name(0), false)
      --     end
      --   end,
      -- })
      vim.keymap.set('n', '<leader>e', '<cmd>lua MiniFiles.open(vim.api.nvim_buf_get_name(0), false)<cr>')
    end,
  },

  {
    'LunarVim/bigfile.nvim',
    lazy = false,
    config = function()
      require('bigfile').setup {
        filesize = 2, -- size of the file in MiB, the plugin round file sizes to the closest MiB
        pattern = { '*' }, -- autocmd pattern or function see <### Overriding the detection of big files>
        features = { -- features to disable
          'indent_blankline',
          'illuminate',
          'lsp',
          'treesitter',
          'syntax',
          'matchparen',
          'vimopts',
          'filetype',
        },
      }
    end,
  },

  { 'echasnovski/mini.bufremove', version = false },

  {
    'folke/which-key.nvim',
    lazy = false,
    opts = {},
  },

  {
    'nvim-neo-tree/neo-tree.nvim',
    version = '*',
    lazy = false,
    dependencies = {
      'nvim-lua/plenary.nvim',
      'nvim-tree/nvim-web-devicons', -- not strictly required, but recommended
      'MunifTanjim/nui.nvim',
    },
    config = function()
      require('neo-tree').setup {
        close_if_last_window = true,
        enable_git_status = true,
        enable_diagnostics = true,

        window = {
          position = 'left',
          width = 30,
          mapping_options = {
            noremap = true,
            nowait = true,
          },
        },
        filesystem = {
          filtered_items = {
            visible = true, -- when true, they will just be displayed differently than normal items
            hide_dotfiles = false,
            hide_gitignored = false,
            hide_hidden = false,
          },
          follow_current_file = {
            enabled = false,
          },
          never_show = { -- remains hidden even if visible is toggled to true, this overrides always_show
            '.DS_Store',
            'thumbs.db',
          },
          use_libuv_file_watcher = true,
        },
      }

      local keymap = vim.keymap.set

      keymap('n', '<Space>T', '<cmd>Neotree show toggle<CR>', { desc = '[t]oggle Neotree window' })
      keymap('n', '<Space>t', '<cmd>Neotree reveal toggle<CR>', { desc = '[T]oggle Neotree active buffer file' })
    end,
  },
}
