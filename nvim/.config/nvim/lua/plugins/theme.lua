---@diagnostic disable: missing-fields
local M = {}
local hl = vim.api.nvim_set_hl

vim.opt.termguicolors = true

function M.trans()
  -- transparent background
  hl(0, 'Normal', { bg = 'NONE', ctermbg = 'NONE' })
  hl(0, 'NormalFloat', { bg = 'none' })
  hl(0, 'TelescopeBorder', { bg = 'NONE', ctermbg = 'NONE' })
  hl(0, 'TelescopePromptBorder', { bg = 'NONE', ctermbg = 'NONE' })
  hl(0, 'TelescopePreviewBorder', { bg = 'NONE', ctermbg = 'NONE' })
  hl(0, 'TelescopeResultsBorder', { bg = 'NONE', ctermbg = 'NONE' })
  hl(0, 'WinSeparator', { bg = 'NONE', ctermbg = 'NONE', fg = 'NONE', ctermfg = 'NONE' })
  hl(0, 'LineNr', { bg = 'NONE', ctermbg = 'NONE' })
  hl(0, 'SignColumn', { bg = 'NONE', ctermbg = 'NONE' })
  hl(0, 'Pmenu', { bg = 'NONE', ctermbg = 'NONE' })
  hl(0, 'PmenuSel', { bg = 'NONE', ctermbg = 'NONE' })
  hl(0, 'vertsplit', { bg = 'NONE', ctermbg = 'NONE' })
  hl(0, 'MsgArea', { bg = 'NONE', ctermbg = 'NONE' })

  hl(0, 'HoverNormal', { bg = 'NONE', ctermbg = 'NONE', fg = 'NONE', ctermfg = 'NONE' })
  hl(0, 'NormalFloat', { bg = 'NONE', ctermbg = 'NONE', fg = 'NONE', ctermfg = 'NONE' })
  hl(0, 'Float', { bg = 'NONE', ctermbg = 'NONE', fg = 'NONE', ctermfg = 'NONE' })
  hl(0, 'FloatBorder', { bg = 'NONE', ctermbg = 'NONE', fg = 'NONE', ctermfg = 'NONE' })
end

function M.setup()
  -- M.trans()

  vim.opt.listchars = {
    eol = '↲',
    nbsp = '◡',
    conceal = '∫',
    extends = '≯',
    precedes = '≮',
    tab = '  ',
  }

  vim.opt.list = true
  vim.opt.termguicolors = true
  vim.opt.winblend = 0
  vim.opt.pumblend = 0
  vim.opt.cmdheight = 0

  hl(0, 'CopilotSuggestion', { fg = '#ff42cf' })
  hl(0, 'CopilotAnnotation', { fg = '#ff42cf' })
  hl(0, 'FlashLabel', { fg = '#ff42cf' })
  -- hl(0, 'LspInlayHint', { fg = '#94ff83' })

  hl(0, 'HoverNormal', { bg = 'NONE', ctermbg = 'NONE', fg = 'NONE', ctermfg = 'NONE' })
  hl(0, 'NormalFloat', { bg = 'NONE', ctermbg = 'NONE', fg = 'NONE', ctermfg = 'NONE' })
  hl(0, 'Float', { bg = 'NONE', ctermbg = 'NONE', fg = 'NONE', ctermfg = 'NONE' })
  hl(0, 'FloatBorder', { bg = 'NONE', ctermbg = 'NONE', fg = 'NONE', ctermfg = 'NONE' })

  -- change Function to be pink
  -- hl(0, "Function", { fg = "#ff2afc" })
end

return {
  {
    '0xstepit/flow.nvim',
    lazy = false,
    enabled = true,
    priority = 1000,
    opts = {},
    config = function()
      require('flow').setup {
        dark_theme = true, -- Set the theme with dark background.
        high_contrast = false, -- Make the dark background darker or the light background lighter.
        transparent = true, -- Set transparent background.
        fluo_color = 'pink', -- Color used as fluo. Available values are pink, yellow, orange, or green.
        -- mode = 'base', -- Mode of the colors. Available values are: dark, bright, desaturate, or base.
        aggressive_spell = false, -- Use colors for spell check.
      }

      -- vim.cmd 'colorscheme flow'
    end,
  },
  {
    'catppuccin/nvim',
    name = 'catppuccin',
    lazy = false,
    enabled = true,
    priority = 1000,
    config = function()
      require('catppuccin').setup {
        integrations = {
          -- Special integrations, see https://github.com/catppuccin/nvim#special-integrations
          barbecue = {
            dim_dirname = true,
            bold_basename = true,
            dim_context = true,
            alt_background = false,
          },
          dap = {
            enabled = true,
            enable_ui = true,
          },
          indent_blankline = {
            enabled = false,
            colored_indent_levels = false,
          },
          native_lsp = {
            enabled = true,
            virtual_text = {
              errors = { 'italic' },
              hints = { 'italic' },
              warnings = { 'italic' },
              information = { 'italic' },
            },
            underlines = {
              errors = { 'underline' },
              hints = { 'underline' },
              warnings = { 'underline' },
              information = { 'underline' },
            },
          },
          navic = {
            enabled = true,
            custom_bg = 'NONE',
          },
        },
      }
      vim.cmd 'colorscheme catppuccin-mocha'
    end,
  },

  {
    'slugbyte/lackluster.nvim',
    enabled = true,
    lazy = false,
    priority = 1000,
    init = function()
      local lackluster = require 'lackluster'

      local color = lackluster.color -- blue, green, red, orange, black, lack, luster, gray1-9

      -- !must called setup() before setting the colorscheme!
      lackluster.setup {
        -- You can overwrite the following syntax colors by setting them to one of...
        --   1) a hexcode like "#a1b2c3" for a custom color
        --   2) "default" or nil will just use whatever lackluster's default is.
        tweak_syntax = {
          string = 'default',
          -- string = "#a1b2c3", -- custom hexcode
          -- Function = '#ff00ff', -- lackluster color
          string_escape = 'default',
          comment = 'default',
          builtin = 'default', -- builtin modules and functions
          type = 'default',
          keyword = 'default',
          keyword_return = 'default',
          keyword_exception = 'default',
        },
        -- You can overwrite the following background colors by setting them to one of...
        --   1) a hexcode like "#a1b2c3" for a custom color
        --   2) "none" for transparency
        --   3) "default" or nil will just use whatever lackluster's default is.
        tweak_background = {
          normal = 'default', -- main background
          -- normal = 'none',    -- transparent
          -- normal = '#a1b2c3',    -- hexcode
          -- normal = color.green,    -- lackluster color
          telescope = 'default', -- telescope
          -- telescope = 'none', -- telescope
          menu = 'default', -- nvim_cmp, wildmenu ... (bad idea to transparent)
          popup = 'default', -- lazy, mason, whichkey ... (bad idea to transparent)
        },
      }

      -- !must set colorscheme after calling setup()!
      -- vim.cmd.colorscheme 'lackluster'
      vim.cmd.colorscheme 'lackluster-mint' -- my favorite
      M.setup()
      -- vim.cmd.colorscheme("lackluster-mint")
    end,
  },
  {
    'norcalli/nvim-colorizer.lua',
    {
      'norcalli/nvim-terminal.lua',
      config = function()
        require('terminal').setup()
      end,
    },
  },

  {
    'echasnovski/mini.icons',
    version = false,
    lazy = false,
    event = 'VeryLazy',
    config = function()
      require('mini.icons').setup()
    end,
  },

  {
    'stevearc/dressing.nvim',
    event = 'VeryLazy',
    opts = {
      input = { relative = 'editor' },
      select = {
        backend = { 'telescope', 'fzf', 'builtin' },
      },
    },
  },
  {
    'echasnovski/mini.indentscope',
    lazy = true,
    event = 'VeryLazy', -- "BufReadPre", "BufNewFile
    version = '*',
    config = function()
      require('mini.indentscope').setup { symbol = '│', options = { try_as_border = false } }
    end,
  },

  {
    'mvllow/modes.nvim',
    lazy = false,
    tag = 'v0.2.0',
    config = function()
      require('modes').setup {
        colors = {
          copy = '#f5c359',
          delete = '#c75c6a',
          insert = '#78ccc5',
          visual = '#9745be',
        },

        -- Set opacity for cursorline and number background
        line_opacity = {
          copy = 0.2,
          delete = 0,
          insert = 0.3,
          visual = 0.6,
        },

        -- Enable cursor highlights
        set_cursor = true,

        -- Enable cursorline initially, and disable cursorline for inactive windows
        -- or ignored filetypes
        set_cursorline = true,

        -- Enable line number highlights to match cursorline
        set_number = true,

        -- Disable modes highlights in specified filetypes
        -- Please PR commonly ignored filetypes
        ignore_filetypes = { 'NvimTree', 'TelescopePrompt' },
      }
    end,
  },

  {
    'goolord/alpha-nvim',
    event = 'VimEnter',
    dependencies = {
      'nvim-tree/nvim-web-devicons',
      'nvim-lua/plenary.nvim',
      -- { 'MaximilianLloyd/ascii.nvim', dependencies = { 'MunifTanjim/nui.nvim' } },
    },
    config = function()
      -- local ascii = require 'ascii'
      local alpha = require 'alpha'
      local dashboard = require 'alpha.themes.startify'

      dashboard.section.header.val = {
        '                                                     ',
        '  ███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗ ',
        '  ████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║ ',
        '  ██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║ ',
        '  ██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║ ',
        '  ██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║ ',
        '  ╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝ ',
        '                                                     ',
      }
      dashboard.file_icons.provider = 'devicons'
      -- dashboard.section.header.val = ascii.art.gaming.doom.cacodemon
      -- dashboard.section.header.val = ascii.get_random_global()
      alpha.setup(dashboard.opts)

      -- Disable folding on alpha buffer
      vim.cmd [[autocmd FileType alpha setlocal nofoldenable]]
    end,
  },
}
