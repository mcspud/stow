require('luasnip.session.snippet_collection').clear_snippets 'elixir'

local ls = require 'luasnip'

local fmta = require('luasnip.extras.fmt').fmta
local rep = require('luasnip.extras').rep

local s = ls.snippet
local c = ls.choice_node
local d = ls.dynamic_node
local i = ls.insert_node
local t = ls.text_node
local sn = ls.snippet_node

-- - [ ] snipped for dbg(t, printable_limit: :infinity)

local snippets = {
  ls.parser.parse_snippet('heex', 'def render(assigns) do \n~H"""\n""" \nend'),

  ls.parser.parse_snippet('mount', 'def mount(_params, _session, socket) do \n{:ok, socket} \nend'),

  ls.add_snippets('elixir', {
    s(
      'env',
      fmta(
        [[
        if Mix.env() != :test do
         <code>
        end
        ]],
        { code = i(1) }
      )
    ),
    s(
      'dbg',
      fmta(
        [[
        dbg(<code>, printable_limit: :infinity)
        ]],
        { code = i(1) }
      )
    ),
  }),
}

return snippets
