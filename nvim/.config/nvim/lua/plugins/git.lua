local M = {}

M.mappings = {
  mappings = {
    issue = {
      close_issue = { lhs = '<localleader>oic', desc = 'close issue' },
      reopen_issue = { lhs = '<localleader>oio', desc = 'reopen issue' },
      list_issues = { lhs = '<localleader>oil', desc = 'list open issues on same repo' },
      reload = { lhs = '<C-r>', desc = 'reload issue' },
      open_in_browser = { lhs = '<C-b>', desc = 'open issue in browser' },
      copy_url = { lhs = '<C-y>', desc = 'copy url to system clipboard' },
      add_assignee = { lhs = '<localleader>oaa', desc = 'add assignee' },
      remove_assignee = { lhs = '<localleader>oad', desc = 'remove assignee' },
      create_label = { lhs = '<localleader>olc', desc = 'create label' },
      add_label = { lhs = '<localleader>ola', desc = 'add label' },
      remove_label = { lhs = '<localleader>old', desc = 'remove label' },
      goto_issue = { lhs = '<localleader>ogi', desc = 'navigate to a local repo issue' },
      add_comment = { lhs = '<localleader>oca', desc = 'add comment' },
      delete_comment = { lhs = '<localleader>ocd', desc = 'delete comment' },
      next_comment = { lhs = ']c', desc = 'go to next comment' },
      prev_comment = { lhs = '[c', desc = 'go to previous comment' },
      react_hooray = { lhs = '<localleader>orp', desc = 'add/remove 🎉 reaction' },
      react_heart = { lhs = '<localleader>orh', desc = 'add/remove ❤️ reaction' },
      react_eyes = { lhs = '<localleader>ore', desc = 'add/remove 👀 reaction' },
      react_thumbs_up = { lhs = '<localleader>or+', desc = 'add/remove 👍 reaction' },
      react_thumbs_down = { lhs = '<localleader>or-', desc = 'add/remove 👎 reaction' },
      react_rocket = { lhs = '<localleader>orr', desc = 'add/remove 🚀 reaction' },
      react_laugh = { lhs = '<localleader>orl', desc = 'add/remove 😄 reaction' },
      react_confused = { lhs = '<localleader>orc', desc = 'add/remove 😕 reaction' },
    },
    pull_request = {
      checkout_pr = { lhs = '<localleader>opo', desc = 'checkout PR' },
      merge_pr = { lhs = '<localleader>opm', desc = 'merge commit PR' },
      squash_and_merge_pr = { lhs = '<localleader>opsm', desc = 'squash and merge PR' },
      list_commits = { lhs = '<localleader>opc', desc = 'list PR commits' },
      list_changed_files = { lhs = '<localleader>opf', desc = 'list PR changed files' },
      show_pr_diff = { lhs = '<localleader>opd', desc = 'show PR diff' },
      add_reviewer = { lhs = '<localleader>ova', desc = 'add reviewer' },
      remove_reviewer = { lhs = '<localleader>ovd', desc = 'remove reviewer request' },
      close_issue = { lhs = '<localleader>oic', desc = 'close PR' },
      reopen_issue = { lhs = '<localleader>oio', desc = 'reopen PR' },
      list_issues = { lhs = '<localleader>oil', desc = 'list open issues on same repo' },
      reload = { lhs = '<C-r>', desc = 'reload PR' },
      open_in_browser = { lhs = '<C-b>', desc = 'open PR in browser' },
      copy_url = { lhs = '<C-y>', desc = 'copy url to system clipboard' },
      goto_file = { lhs = 'gf', desc = 'go to file' },
      add_assignee = { lhs = '<localleader>oaa', desc = 'add assignee' },
      remove_assignee = { lhs = '<localleader>oad', desc = 'remove assignee' },
      create_label = { lhs = '<localleader>olc', desc = 'create label' },
      add_label = { lhs = '<localleader>ola', desc = 'add label' },
      remove_label = { lhs = '<localleader>old', desc = 'remove label' },
      goto_issue = { lhs = '<localleader>ogi', desc = 'navigate to a local repo issue' },
      add_comment = { lhs = '<localleader>oca', desc = 'add comment' },
      delete_comment = { lhs = '<localleader>ocd', desc = 'delete comment' },
      next_comment = { lhs = ']c', desc = 'go to next comment' },
      prev_comment = { lhs = '[c', desc = 'go to previous comment' },
      react_hooray = { lhs = '<localleader>orp', desc = 'add/remove 🎉 reaction' },
      react_heart = { lhs = '<localleader>orh', desc = 'add/remove ❤️ reaction' },
      react_eyes = { lhs = '<localleader>ore', desc = 'add/remove 👀 reaction' },
      react_thumbs_up = { lhs = '<localleader>or+', desc = 'add/remove 👍 reaction' },
      react_thumbs_down = { lhs = '<localleader>or-', desc = 'add/remove 👎 reaction' },
      react_rocket = { lhs = '<localleader>orr', desc = 'add/remove 🚀 reaction' },
      react_laugh = { lhs = '<localleader>orl', desc = 'add/remove 😄 reaction' },
      react_confused = { lhs = '<localleader>orc', desc = 'add/remove 😕 reaction' },
    },
    review_thread = {
      goto_issue = { lhs = '<localleader>ogi', desc = 'navigate to a local repo issue' },
      add_comment = { lhs = '<localleader>oca', desc = 'add comment' },
      add_suggestion = { lhs = '<localleader>osa', desc = 'add suggestion' },
      delete_comment = { lhs = '<localleader>ocd', desc = 'delete comment' },
      next_comment = { lhs = ']c', desc = 'go to next comment' },
      prev_comment = { lhs = '[c', desc = 'go to previous comment' },
      select_next_entry = { lhs = ']q', desc = 'move to previous changed file' },
      select_prev_entry = { lhs = '[q', desc = 'move to next changed file' },
      close_review_tab = { lhs = '<C-c>', desc = 'close review tab' },
      react_hooray = { lhs = '<localleader>orp', desc = 'add/remove 🎉 reaction' },
      react_heart = { lhs = '<localleader>orh', desc = 'add/remove ❤️ reaction' },
      react_eyes = { lhs = '<localleader>ore', desc = 'add/remove 👀 reaction' },
      react_thumbs_up = { lhs = '<localleader>or+', desc = 'add/remove 👍 reaction' },
      react_thumbs_down = { lhs = '<localleader>or-', desc = 'add/remove 👎 reaction' },
      react_rocket = { lhs = '<localleader>orr', desc = 'add/remove 🚀 reaction' },
      react_laugh = { lhs = '<localleader>orl', desc = 'add/remove 😄 reaction' },
      react_confused = { lhs = '<localleader>orc', desc = 'add/remove 😕 reaction' },
    },
    submit_win = {
      approve_review = { lhs = '<C-a>', desc = 'approve review' },
      comment_review = { lhs = '<C-m>', desc = 'comment review' },
      request_changes = { lhs = '<C-r>', desc = 'request changes review' },
      close_review_tab = { lhs = '<C-c>', desc = 'close review tab' },
    },
    review_diff = {
      add_review_comment = { lhs = '<localleader>oca', desc = 'add a new review comment' },
      add_review_suggestion = { lhs = '<localleader>osa', desc = 'add a new review suggestion' },
      focus_files = { lhs = '<leader>e', desc = 'move focus to changed file panel' },
      toggle_files = { lhs = '<leader>b', desc = 'hide/show changed files panel' },
      next_thread = { lhs = ']t', desc = 'move to next thread' },
      prev_thread = { lhs = '[t', desc = 'move to previous thread' },
      select_next_entry = { lhs = ']q', desc = 'move to previous changed file' },
      select_prev_entry = { lhs = '[q', desc = 'move to next changed file' },
      close_review_tab = { lhs = '<C-c>', desc = 'close review tab' },
      -- toggle_viewed = { lhs = "<leader><localleader>o", desc = "toggle viewer viewed state" },
      goto_file = { lhs = 'gf', desc = 'go to file' },
    },
    file_panel = {
      next_entry = { lhs = 'j', desc = 'move to next changed file' },
      prev_entry = { lhs = 'k', desc = 'move to previous changed file' },
      select_entry = { lhs = '<cr>', desc = 'show selected changed file diffs' },
      refresh_files = { lhs = 'R', desc = 'refresh changed files panel' },
      focus_files = { lhs = '<leader>e', desc = 'move focus to changed file panel' },
      toggle_files = { lhs = '<leader>b', desc = 'hide/show changed files panel' },
      select_next_entry = { lhs = ']q', desc = 'move to previous changed file' },
      select_prev_entry = { lhs = '[q', desc = 'move to next changed file' },
      close_review_tab = { lhs = '<C-c>', desc = 'close review tab' },
      -- toggle_viewed = { lhs = "<leader><localleader>o", desc = "toggle viewer viewed state" },
    },
  },
}

return {
  {
    'sindrets/diffview.nvim',
    cmd = { 'DiffviewOpen', 'DiffviewClose', 'DiffviewToggleFiles', 'DiffviewFocusFiles' },
    config = true,
  },
  {
    'NeogitOrg/neogit',
    lazy = false,
    priority = 1000,
    -- cmd = 'Neogit',
    config = function()
      local neogit = require 'neogit'
      neogit.setup {
        integrations = { diffview = true, telescope = true },
        disable_commit_confirmation = true,
        auto_show_console = false,
        -- kind = "vsplit",
      }

      -- git
      vim.keymap.set('n', '<localleader>g', ':Neogit<CR>')
      vim.keymap.set('n', '<localleader>h', ':GlLog<CR>')

      vim.keymap.set('n', '<leader>ga', '<Cmd>Telescope repo list<Cr>', { desc = 'Git All repositories' })
      vim.keymap.set('n', '<leader>gj', "<cmd>lua require 'gitsigns'.next_hunk()<cr>", { desc = 'Git Next Hunk' })
      vim.keymap.set('n', '<leader>gk', "<cmd>lua require 'gitsigns'.prev_hunk()<cr>", { desc = 'Git Prev Hunk' })
      vim.keymap.set('n', '<leader>gs', "<cmd>lua require 'gitsigns'.stage_hunk()<cr>", { desc = 'Git Stage Hunk' })
      vim.keymap.set('n', '<leader>gl', "<cmd>lua require('gitsigns').blame_line()<CR>", { desc = 'Git Blame' })
      vim.keymap.set('n', '<leader>go', ':Octo actions<cr>', { desc = 'Git Octo actions' })
      vim.keymap.set('n', '<leader>gv', '<Cmd>DiffviewOpen<Cr>', { desc = 'Git Diffview open' })
      vim.keymap.set('n', '<leader>gc', '<Cmd>DiffviewClose<Cr>', { desc = 'Git Diffview close' })
      vim.keymap.set(
        'n',
        '<leader>gy',
        "<cmd>lua require'gitlinker'.get_buf_range_url('n', {action_callback = require'gitlinker.actions'.open_in_browser})<cr>",
        { desc = 'Git Link' }
      )
      vim.keymap.set('n', '<leader>gt', "<cmd>lua require('telescope').extensions.advanced_git_search.show_custom_functions()<cr>", { desc = 'Git Telescope' })

      -- Worktree submenu
      vim.keymap.set(
        'n',
        '<leader>gwc',
        "<cmd>lua require('telescope').extensions.git_worktree.create_git_worktree(require('telescope.themes').get_ivy({}))<cr>",
        { desc = 'Git Create Worktree' }
      )
      vim.keymap.set(
        'n',
        '<leader>gww',
        "<cmd>lua require('telescope').extensions.git_worktree.git_worktrees(require('telescope.themes').get_ivy({}))<cr>",
        { desc = 'Git List Worktrees' }
      )
    end,
  },
  {
    'lewis6991/gitsigns.nvim',
    event = 'BufReadPre',
    lazy = true,
    priority = 1000,
    config = function()
      require('gitsigns').setup()
    end,
  },
  {
    'aaronhallaert/advanced-git-search.nvim',
    config = function()
      require('telescope').load_extension 'advanced_git_search'
    end,
    dependencies = {
      'nvim-telescope/telescope.nvim',
      'tpope/vim-fugitive',
    },
  },
  -- {
  --   "pwntester/octo.nvim",
  --   lazy = true,
  --   event = "VeryLazy",
  --   dependencies = {
  --     "nvim-lua/plenary.nvim",
  --     "nvim-telescope/telescope.nvim",
  --   },
  --   config = function()
  --     require("octo").setup {
  --       require("plugins.git").mappings,
  --     }
  --   end,
  -- },

  {
    'akinsho/git-conflict.nvim',
    config = function()
      require('git-conflict').setup()
    end,
  },
  'AndrewRadev/diffurcate.vim', -- see fish
  {
    'ThePrimeagen/git-worktree.nvim',
    config = function()
      require('git-worktree').setup()
    end,
  },
  {
    'ruifm/gitlinker.nvim',
    dependencies = 'nvim-lua/plenary.nvim',
    module = 'gitlinker',
    config = function()
      require('gitlinker').setup { mappings = nil }
    end,
  },
}
