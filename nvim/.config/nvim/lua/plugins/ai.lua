return {

  {
    'CopilotC-Nvim/CopilotChat.nvim',
    enabled = false,
    lazy = true,
    event = 'VeryLazy',
    branch = 'canary',
    dependencies = {
      { 'zbirenbaum/copilot.lua' }, -- or github/copilot.vim
      { 'nvim-lua/plenary.nvim' }, -- for curl, log wrapper
    },
    opts = {
      debug = true, -- Enable debugging
      auto_follow_cursor = false, -- Auto-follow cursor in chat
      context = 'buffers', -- Default context to use, 'buffers', 'buffer' or none (can be specified manually in prompt via @).
      -- history_path = vim.fn.stdpath 'data' .. '/copilotchat_history', -- Default path to stored history
      -- callback = nil, -- Callback to use when ask response is received
      --
      -- See Configuration section for rest
      prompts = {
        -- Mansplain = {
        --   prompt = 'Explain how it works.',
        --   -- mapping = '<leader>ccmc',
        --   description = 'My custom prompt description',
        --   -- selection = require('CopilotChat.select').visual,
        -- },
      },
    },
    keys = {
      -- {
      --   '<leader>lg',
      --   function()
      --     local actions = require 'CopilotChat.actions'
      --     require('CopilotChat.integrations.telescope').pick(actions.prompt_actions())
      --   end,
      --   desc = 'CopilotChat - Prompt actions',
      -- },

      -- vim.keymap.set('n', '<leader>lg', '<cmd>:Gen<CR>', { desc = 'Gen AI Ollama' })
    },
    -- See Commands section for default commands if you want to lazy load on them
  },

  {
    'zbirenbaum/copilot.lua',
    cmd = 'Copilot',
    enabled = false,
    event = 'InsertEnter',
    config = function()
      require('copilot').setup {
        panel = {
          enabled = false,
          auto_refresh = false,
          keymap = {
            jump_prev = '[[',
            jump_next = ']]',
            accept = '<C-y>',
            refresh = 'gr',
            open = '<M-CR>',
          },
          layout = {
            position = 'bottom', -- | top | left | right
            ratio = 0.4,
          },
        },
        suggestion = {
          enabled = false,
          auto_trigger = true,
          debounce = 75,
          keymap = {
            accept = '<C-l>',
            accept_word = false,
            accept_line = false,
            next = '<M-]>',
            prev = '<M-[>',
            dismiss = '<C-]>',
          },
        },
      }

      vim.keymap.set('n', '<leader>lg', function()
        local copilot_actions = require 'CopilotChat.actions'
        require('CopilotChat.integrations.telescope').pick(copilot_actions.prompt_actions())
      end, { desc = 'AI Commands' })
    end,
  },
  {
    'zbirenbaum/copilot-cmp',
    enabled = false,
    lazy = true,
    event = 'InsertEnter',
    config = function()
      require('copilot_cmp').setup()
    end,
  },
}
