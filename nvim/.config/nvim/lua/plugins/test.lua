return {
  {
    'vim-test/vim-test',
    config = function()
      vim.g['test#strategy'] = 'neovim'
      vim.g['test#neovim#term_position'] = 'belowright'
      vim.g['test#neovim#preserve_screen'] = 1
      vim.g['test#python#runner'] = 'pyunit' -- pytest
    end,
  },
  {
    'nvim-neotest/neotest',
    lazy = false,
    keys = {
      { '<leader>nF', "<cmd>w|lua require('neotest').run.run({vim.fn.expand('%'), strategy = 'dap'})<cr>", desc = 'Test Debug File' },
      { '<leader>nL', "<cmd>w|lua require('neotest').run.run_last({strategy = 'dap'})<cr>", desc = 'Test Debug Last' },
      { '<leader>na', "<cmd>w|lua require('neotest').run.attach()<cr>", desc = 'Test Attach' },
      { '<leader>nf', "<cmd>w|lua require('neotest').run.run(vim.fn.expand('%'))<cr>", desc = 'Test File' },
      { '<leader>nl', "<cmd>w|lua require('neotest').run.run_last()<cr>", desc = 'Test Last' },
      { '<leader>nn', "<cmd>w|lua require('neotest').run.run()<cr>", desc = 'Test Nearest' },
      { '<leader>nN', "<cmd>w|lua require('neotest').run.run({strategy = 'dap'})<cr>", desc = 'Test Debug Nearest' },
      { '<leader>no', "<cmd>w|lua require('neotest').output_panel.open({ enter = true })<cr>", desc = 'Test Output' },
      { '<leader>ns', "<cmd>w|lua require('neotest').run.stop()<cr>", desc = 'Test Stop' },
      { '<leader>nS', "<cmd>w|lua require('neotest').summary.toggle()<cr>", desc = 'Test Summary' },
    },
    dependencies = {
      'vim-test/vim-test',
      'nvim-neotest/neotest-python',
      'nvim-neotest/neotest-plenary',
      'nvim-neotest/neotest-vim-test',
      'jfpedroza/neotest-elixir',
      'rouge8/neotest-rust',
      'hiphish/neotest-busted',
    },
    config = function()
      local opts = {
        adapters = {
          require 'neotest-elixir' {
            -- The Mix task to use to run the tests
            -- Can be a function to return a dynamic value.
            -- Default: "test"
            -- mix_task = { "test.interactive" },
          },
          require 'neotest-python' {
            dap = { justMyCode = false },
            runner = 'unittest',
          },
          require 'neotest-plenary',
          require 'neotest-vim-test' {
            ignore_file_types = { 'python', 'vim', 'lua' },
          },
          require 'neotest-rust',
          require 'neotest-busted',
        },
        status = { virtual_text = true },
        output = { open_on_run = true },
        quickfix = {
          open = function()
            if require('utils').has 'trouble.nvim' then
              vim.cmd 'Trouble quickfix'
            else
              vim.cmd 'copen'
            end
          end,
        },
        -- overseer.nvim
        consumers = {
          overseer = require 'neotest.consumers.overseer',
        },
        overseer = {
          enabled = true,
          force_default = true,
        },
      }
      require('neotest').setup(opts)
    end,
  },
  {
    'stevearc/overseer.nvim',
    keys = {
      { '<leader>oR', '<cmd>OverseerRunCmd<cr>', desc = 'Overseer Run Command' },
      { '<leader>oa', '<cmd>OverseerTaskAction<cr>', desc = 'Overseer Task Action' },
      { '<leader>ob', '<cmd>OverseerBuild<cr>', desc = 'Overseer Build' },
      { '<leader>oc', '<cmd>OverseerClose<cr>', desc = 'Overseer Close' },
      { '<leader>od', '<cmd>OverseerDeleteBundle<cr>', desc = 'Overseer Delete Bundle' },
      { '<leader>ol', '<cmd>OverseerLoadBundle<cr>', desc = 'Overseer Load Bundle' },
      { '<leader>oo', '<cmd>OverseerOpen<cr>', desc = 'Overseer Open' },
      { '<leader>oq', '<cmd>OverseerQuickAction<cr>', desc = 'Overseer Quick Action' },
      { '<leader>or', '<cmd>OverseerRun<cr>', desc = 'Overseer Run' },
      { '<leader>os', '<cmd>OverseerSaveBundle<cr>', desc = 'Overseer Save Bundle' },
      { '<leader>ot', '<cmd>OverseerToggle<cr>', desc = 'Overseer Toggle' },
    },
    config = true,
  },
}
