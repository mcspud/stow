return {
  {
    -- TODO: v2 https://github.com/ThePrimeagen/harpoon
    'ThePrimeagen/harpoon',
    lazy = true,
    event = 'VeryLazy',
    config = function()
      local keymap = vim.keymap.set
      local default_opts = { noremap = true, silent = true }
      local expr_opts = { noremap = true, expr = true, silent = true }

      keymap('n', ',w', "<Cmd>lua require('harpoon.mark').add_file()<Cr>", default_opts)
      keymap('n', ',e', "<Cmd>lua require('harpoon.ui').toggle_quick_menu()<Cr>", default_opts)
      keymap('n', ',c', "<Cmd>lua require('harpoon.cmd-ui').toggle_quick_menu()<Cr>", default_opts)
      keymap('n', ',a', "<Cmd>lua require('harpoon.ui').nav_file(1)<Cr>", default_opts)
      keymap('n', ',s', "<Cmd>lua require('harpoon.ui').nav_file(2)<Cr>", default_opts)
      keymap('n', ',d', "<Cmd>lua require('harpoon.ui').nav_file(3)<Cr>", default_opts)
      keymap('n', ',f', "<Cmd>lua require('harpoon.ui').nav_file(4)<Cr>", default_opts)

      -- Go to previous file with backspace
      -- keymap('n', '<BS>', ":lua require('harpoon.ui').nav_next()<CR>", default_opts)
    end,
    opts = {
      global_settings = {
        save_on_toggle = true,
        enter_on_sendcmd = true,
      },
    },
  },
}
