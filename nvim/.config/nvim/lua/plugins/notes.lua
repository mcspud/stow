return {
  { 'mzlogin/vim-markdown-toc', ft = { 'markdown' } },
  {
    'OXY2DEV/markview.nvim',
    enabled = true,
    lazy = false, -- Recommended
    -- ft = "markdown" -- If you decide to lazy-load anyway
    config = function()
      require('markview').setup {
        modes = { 'n', 'i', 'no', 'c' },
        hybrid_modes = { 'i' },

        -- This is nice to have
        callbacks = {
          on_enable = function(_, win)
            vim.wo[win].conceallevel = 2
            vim.wo[win].concealcursor = 'nc'
          end,
        },
      }
    end,
  },
}
