local M = {}

function M.setup()
  local dap = require 'dap'

  local elixir_ls_debugger = vim.fn.exepath 'elixir-ls-debugger'
  if elixir_ls_debugger ~= '' then
    dap.adapters.mix_task = {
      type = 'executable',
      command = elixir_ls_debugger,
    }
  end

  dap.configurations.elixir = {
    {
      type = 'mix_task',
      name = 'phoenix server',
      task = 'phx.server',
      request = 'launch',
      projectDir = '${workspaceFolder}',
      exitAfterTaskReturns = false,
      debugAutoInterpretAllModules = false,
    },

    {
      type = 'mix_task',
      name = 'mix test',
      task = 'test',
      taskArgs = { '--trace' },
      request = 'launch',
      startApps = true, -- for Phoenix projects
      projectDir = '${workspaceFolder}',
      requireFiles = {
        'test/**/test_helper.exs',
        'test/**/*_test.exs',
      },
    },

    {
      type = 'test.interactive',
      name = 'mix interactive',
      task = 'test',
      taskArgs = { '--trace' },
      request = 'launch',
      startApps = true, -- for Phoenix projects
      projectDir = '${workspaceFolder}',
    },
    {
      type = 'mix_task',
      name = 'phx.server',
      request = 'launch',
      startApps = false, -- for Phoenix projects
      task = 'phx.server',
      projectDir = '${workspaceRoot}',
    },
  }
end

return M
