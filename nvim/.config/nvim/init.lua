-- Load options configuration if available
pcall(require, 'config.options')

-- Load plugin manager configuration if available
pcall(require, 'config.lazy')

-- Load keymaps configuration if available
pcall(require, 'config.keymaps')
-- Enable vim loader if it's available
-- if vim.loader then
--   vim.loader.enable()
-- end

-- Load autocmds configuration if available
pcall(require, 'config.autocmds')


-- Setup the theme if it's available
local ok, t = pcall(require, 'config.theme')
if ok then
  t.setup()
end
